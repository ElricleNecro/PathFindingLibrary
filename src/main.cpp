#include <cstdlib>
#include <fstream>
#include <iostream>

#include "PathFinder/finder.hpp"

int main(int argc, char *argv[])
{
	if( argc != 3 )
	{
		std::cerr << "Usage: " << argv[0] << " [filename] [OutBufferSize]." << std::endl;
		return -1;
	}

	unsigned int outBufferSize = std::atoi(argv[2]);
	int *outMap;
	unsigned int mapWidth, mapHeight;
	unsigned char *map;
	int lengthPath = 0;

	unsigned int startX  = 0,
		     startY  = 0,
		     targetX = 9,
		     targetY = 4;
	{
		unsigned int tmp;
		std::ifstream mapFile(argv[1], std::ios::in);
		if( !mapFile )
		{
			std::cerr << "Cannot open file '" << argv[1] << "'" << std::endl;
			return -1;
		}

		mapFile >> mapWidth >> mapHeight;

		map = new unsigned char[mapWidth * mapHeight];

		for(unsigned int i = 0; i < mapWidth * mapHeight; ++i)
		{
			mapFile >> tmp;
			map[i] = static_cast<unsigned char>(tmp);
		}

		mapFile.close();
	}

	outMap = new int[outBufferSize];

#ifdef DEBUG_MAP_READ
	for(unsigned int y = 0; y < mapHeight; ++y)
	{
#ifdef DEBUG_MAP_READ_FULL
		std::cout << "y = " << y << "\t";
#endif
		for(unsigned int x = 0; x < mapWidth; ++x)
		{
#ifdef DEBUG_MAP_READ_FULL
			std::cout << "x = " << x << " v = ";
#endif
			std::cout << map[y * mapWidth + x] << " ";
#ifdef DEBUG_MAP_READ_FULL
			std::cout << "| ";
#endif
		}
		std::cout << std::endl;
	}
#endif

	lengthPath = pathfinder::BreadthFirstSearch(
		startX, startY,
		targetX, targetY,
		map, mapWidth, mapHeight,
		outMap, outBufferSize
	);

	std::cout << "We want to find a path of length " << outBufferSize << " at most, between ( " << startX << ", " << startY << " ) and ( " << targetX << ", " << targetY << " ) on the map '" << argv[1] << "'." << std::endl;
	std::cout << "Find a path of length " << lengthPath << std::endl;

	std::ofstream path("results.txt", std::ofstream::out);

	for(int i = 0; i < lengthPath; i+=2)
	{
		path << outMap[i] << " " << outMap[i+1] << std::endl;
	}

	path.close();

	delete[] outMap;
	delete[] map;

	return 0;
}

