#include <iostream>
#include "PathFinder/finder.hpp"

namespace pathfinder
{
	RectangleMap::RectangleMap(const unsigned char *pMap, const int nMapWidth, const int nMapHeight)
	: graph::Graph(pMap, nMapWidth * nMapHeight), map_w(nMapWidth), map_h(nMapHeight)
	{
		this->create();
	}

	std::tuple<int,int> RectangleMap::fromFlattenIdx(const int pos) const
	{
		return std::make_tuple(pos % this->map_w, pos / this->map_w);
	}

	int RectangleMap::toFlattenIdx(const int x, const int y) const
	{
		if( (x < 0 || x >= this->map_w) || (y < 0 || y >= this->map_h) )
			return -1;
		return y * this->map_w + x;
	}

	bool RectangleMap::contain(const int x, const int y) const
	{
		int pos = this->toFlattenIdx(x, y);

		if( pos < 0 || pos >= this->length )
			return false;

		if( this->map[pos] == this->Blocked )
			return false;

		return true;
	}

	int BreadthFirstSearch(
			const int nStartX, const int nStartY,					// Starting point
			const int nTargetX, const int nTargetY,					// Destination point
			const unsigned char* pMap, const int nMapWidth, const int nMapHeight,	// Grid of given size with row-major
			int* pOutBuffer, const int nOutBufferSize				// Output buffer for the result
	)
	{
		// Rewrite everything using an array containing all position as an index (y * mapWidth + x) and
		// containing the index of the comming from places.

		// Creation of the map representation:
		RectangleMap map_repr(pMap, nMapWidth, nMapHeight);

		if( !map_repr.contain(nTargetX, nTargetY) )
			return -1;

		// A queue to track what we still have to go through:
		std::queue<unsigned int> frontier;
		frontier.push(nStartY * nMapWidth + nStartX);

		// We will be tracking the way to go to each point:
		int *came_from = new int[nMapWidth*nMapHeight];
		for(unsigned int i = 0; i < static_cast<unsigned int>(nMapWidth*nMapHeight); ++i)
			came_from[i] = -2;
		came_from[nStartY * nMapWidth + nStartX] = -1;

		// While we have something in the frontier, we have something to do:
		unsigned int current, goal = nTargetY * nMapWidth + nTargetX;
		while( !frontier.empty() )
		{
			// We are getting the first inserted element:
			current = frontier.front();
			if( current == goal )
				break;

			// And we are getting his neighbors:
			auto next = map_repr.getNeighborOf(current);

			for(auto i: *next)
			{
				// Have this node already be walked over?
				if( came_from[i] == -2 )
				{
					// If not, let's add it to the frontier:
					frontier.push(i);
					// And notify from where we came:
					came_from[i] = current;
				}
			}

			// Done with this done, let's remove it:
			frontier.pop();
		}

		current = came_from[nTargetY * nMapWidth + nTargetX];
		std::vector<int> results;
		while(static_cast<unsigned long>(nOutBufferSize) > ( results.size()*2 ) && came_from[current] != -1)
		{
			results.push_back(current);
			current = came_from[current];
		}

		std::reverse(results.begin(), results.end());
		for(int i = 0, j=0; i < nOutBufferSize; i+=2,j++)
		{
			pOutBuffer[i] = results[j] % nMapWidth;
			pOutBuffer[i+1] = results[j] / nMapWidth;
		}

		delete[] came_from;

		return results.size() * 2;
	}

	int FindPath(
			const int nStartX, const int nStartY,					// Starting point
			const int nTargetX, const int nTargetY,					// Destination point
			const unsigned char* pMap, const int nMapWidth, const int nMapHeight,	// Grid of given size with row-major
			int* pOutBuffer, const int nOutBufferSize				// Output buffer for the result
	)
	{
		(void)nStartX;
		(void)nStartY;
		(void)nTargetX;
		(void)nTargetY;
		(void)pMap;
		(void)nMapWidth;
		(void)nMapHeight;
		(void)pOutBuffer;
		(void)nOutBufferSize;
		return -1;
	}
} /* pathfinder */
