#include "Graph/graph.hpp"

failed_alloc::failed_alloc(void)
: err(std::strerror(errno))
{
}

failed_alloc::~failed_alloc(void)
{
}

const char* failed_alloc::what(void) const noexcept
{
	return this->err;
}

namespace graph
{
	Graph::Graph(const unsigned char *map, const int length)
	: Blocked(0), Passable(0), node_lst(nullptr), map(map), length(length)
	{
		// Allocationg the list of all node:
		this->node_lst = new Node[this->length];
	}

	Graph::~Graph(void)
	{
		delete[] this->node_lst;
	}

	void Graph::create(void)
	{
		// And now filling it:
		// TODO: Coordinate are actually wrong. Now corrected?
		for(int i = 0; i < this->length; ++i)
		{
			// The node coordinate:
			std::tie(this->node_lst[i].x, this->node_lst[i].y) = this->fromFlattenIdx(i);
			// this->node_lst[i].x = i % this->map_w; // x -> width
			// this->node_lst[i].y = i / this->map_w; // y -> height
			this->node_lst[i].id = i; // index on the graph.

			this->node_lst[i].setNeighborVector(MAX_NUMBER_OF_NEIGHBORS);

			// We are getting its neighbors:
			this->initNeighbors(this->node_lst[i]);
		}
	}

	std::tuple<int,int> Graph::fromFlattenIdx(const int pos) const
	{
		return std::make_tuple(-1, -1);
		(void)pos;
	}

	int Graph::toFlattenIdx(const int x, const int y) const
	{
		return -1;
		(void)x;
		(void)y;
	}

	bool Graph::contain(const int x, const int y) const
	{
		return false;
		(void)x;
		(void)y;
	}

	// We want the neighbors of a particular node:
	const NeighborVector* Graph::getNeighborOf(int idx) const
	{
		// Getting the neighbors of node (x, y):
		 return &this->node_lst[idx].neighbors;
	}
	const NeighborVector* Graph::getNeighborOf(int x, int y) const
	{
		// Getting the neighbors of node (x, y):
		 return &this->node_lst[this->toFlattenIdx(x, y)].neighbors;
		 // return &this->node_lst[y * this->map_w + x].neighbors;
	}

	// We are building the node 'node' neightbors:
	void Graph::initNeighbors(Node &node)
	{
		// We are in 2D, with no diagonals connection. Our neighbor are:
		int directions[4][2] = {
			{1, 0},
			{0, 1},
			{-1, 0},
			{0, -1},
		};
		int x, y;

		// we are going from a list of (x, y) couple to a 1D array, so two index are needed:
		for(int i = 0; i < 4; ++i)
		{
			// Calculating the neighbor coordinate:
			x = node.x + directions[i][0];
			y = node.y + directions[i][1];

			// Is it a valid set of coordinate:
			if( this->contain(x, y) )
			{
				node.neighbors.append(this->toFlattenIdx(x, y));
				// node.neighbors.append(y * this->map_w + x);
			} // This way is less efficient than comparing to width and height, but it's independant
			//   of the geometry of the map.
		}
	}
} /* graph */
