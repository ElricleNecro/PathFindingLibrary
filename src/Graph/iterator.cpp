#include "Graph/iterator.hpp"

namespace graph
{
	NeighborVectorIter::NeighborVectorIter(const NeighborVector *vec, int pos)
	: pos(pos), vec(vec)
	{
	}

	NeighborVectorIter::~NeighborVectorIter(void)
	{
	}

	int NeighborVectorIter::operator*(void) const
	{
		return this->vec->neighbor[this->pos];
	}

	bool NeighborVectorIter::operator!=(const NeighborVectorIter &other)
	{
		return pos != other.pos;
	}

	const NeighborVectorIter& NeighborVectorIter::operator++(void)
	{
		pos++;
		return *this;
	}
} /* graph */
