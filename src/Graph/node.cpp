#include "Graph/node.hpp"

namespace graph
{
	NeighborVector::NeighborVector(void)
	: nbNeighbor(0), effectiveNeighbor(0), neighbor(nullptr)
	{
	}

	NeighborVector::NeighborVector(const int nbNeighbor)
	: nbNeighbor(nbNeighbor), effectiveNeighbor(0), neighbor(nullptr)
	{
		this->neighbor = new int[nbNeighbor];
	}

	NeighborVector::~NeighborVector(void)
	{
		delete[] this->neighbor;
	}

	void NeighborVector::init(const int nbNeighbor)
	{
		this->nbNeighbor = nbNeighbor;
		this->neighbor = new int[nbNeighbor];
	}

	int NeighborVector::count(void) const
	{
		return this->effectiveNeighbor;
	}

	bool NeighborVector::append(const int pos)
	{
		if( this->effectiveNeighbor == this->nbNeighbor )
			return false;

		this->neighbor[this->effectiveNeighbor] = pos;
		this->effectiveNeighbor++;

		return true;
	}

	NeighborVectorIter NeighborVector::begin(void) const
	{
		return NeighborVectorIter(this, 0);
	}

	NeighborVectorIter NeighborVector::end(void) const
	{
		return NeighborVectorIter(this, this->effectiveNeighbor);
	}

	Node::Node(void)
	{
	}

	Node::Node(const int nbNeighbor)
	: neighbors(nbNeighbor)
	{
	}

	void Node::setNeighborVector(const int nbNeighbor)
	{
		 this->neighbors.init(nbNeighbor);
	}
} /* graph */
