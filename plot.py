# coding: utf-8
from matplotlib import pyplot as plt
import numpy as np
map = np.loadtxt("map_test.txt", skiprows=1, dtype=np.int)
path = np.loadtxt("results.txt", dtype=np.int)
plt.imshow(map, interpolation="nearest")
plt.plot(path[:,0], path[:,1], "ko")
plt.plot([0], [0], "yx")
plt.plot([9], [4], "yx")
plt.savefig("pathfinding.png", transparent=True)
