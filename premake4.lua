-- io.input("config.h.cmake")
-- local text = io.read("*a")

-- text = string.gsub(text, "@PLUGIN_API_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@PLUGIN_API_VERSION_MINOR@", "1")

-- text = string.gsub(text, "@AI_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@AI_VERSION_MINOR@", "1")

-- io.output("include/config.hpp")
-- io.write(text)
-- io.close()

solution("ParadoxTest")
	configurations({"debug", "release"})
		buildoptions(
			{
				"-std=c++11"
			}
		)

		flags(
			{
				"ExtraWarnings"
			}
		)

	includedirs(
		{
			"include/"
		}
	)

	configuration("debug")
		buildoptions(
			{
				"-g"
			}
		)
		flags(
			{
				"Symbols"
			}
		)

	configuration("release")
		buildoptions(
			{
				"-O3"
			}
		)

	project("pathfinding")
		language("C++")
		kind("ConsoleApp")

		location("build/bin")
		targetdir("build/bin")

		files(
			{
				"src/*.cpp"
			}
		)

		links(
			{
				"PathFinder"
				, "Graph"
			}
		)
		libdirs(
			{
				"build/lib"
			}
		)

	project("PathFinder")
		language("C++")
		kind("SharedLib")

		location("build/lib")
		targetdir("build/lib")

		files(
			{
				"src/PathFinder/*.cpp"
			}
		)
		links(
			{
				"Graph"
				-- , "Queue"
			}
		)
		libdirs(
			{
				"build/lib"
			}
		)

	project("Graph")
		language("C++")
		kind("SharedLib")

		location("build/lib")
		targetdir("build/lib")

		files(
			{
				"src/Graph/*.cpp"
			}
		)

	-- project("Queue")
		-- language("C++")
		-- kind("SharedLib")

		-- location("build/lib")
		-- targetdir("build/lib")

		-- files(
			-- {
				-- "src/Queue/*.cpp"
			-- }
		-- )
