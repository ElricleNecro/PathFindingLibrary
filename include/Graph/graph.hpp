#ifndef GRAPH_HPP_I86YUSQR
#define GRAPH_HPP_I86YUSQR

// Some C header I need:
#include <cerrno>
#include <cstring>
#include <cstdlib>

// And now the C++ header:
#include <tuple>
#include <exception>

#include "Graph/node.hpp"

#ifndef MAX_NUMBER_OF_NEIGHBORS
#define MAX_NUMBER_OF_NEIGHBORS 4
#endif

// #ifndef PASSABLE
// #define PASSABLE (unsigned char)1
// #endif

// #ifndef BLOCKED
// #define BLOCKED (unsigned char)0
// #endif

class failed_alloc : public std::exception
{
	public:
		failed_alloc(void);
		virtual ~failed_alloc(void);

		virtual const char* what(void) const noexcept;

	private:
		const char *err;
};

namespace graph
{
	struct Node;
	class NeighborVector;

	class Graph
	{
		public:
			Graph(const unsigned char *map, const int length);
			~Graph(void);

			void create(void);

			virtual std::tuple<int,int> fromFlattenIdx(const int pos) const = 0;
			virtual int toFlattenIdx(const int x, const int y) const = 0;

			virtual bool contain(const int x, const int y) const = 0;

			const NeighborVector* getNeighborOf(int idx) const;
			const NeighborVector* getNeighborOf(int x, int y) const;

			unsigned char Blocked;
			unsigned char Passable;

		private:
			void initNeighbors(Node &node);

			friend class NeighborVector;

		protected:
			Node *node_lst;
			const unsigned char *map;
			const int length;
	};
} /* graph */

#endif /* end of include guard: GRAPH_HPP_I86YUSQR */
