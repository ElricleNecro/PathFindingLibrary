#ifndef ITERATOR_HPP_XNI0FOU1
#define ITERATOR_HPP_XNI0FOU1

#include "Graph/node.hpp"

namespace graph
{
	class NeighborVector;

	class NeighborVectorIter
	{
		public:
			NeighborVectorIter(const NeighborVector *vec, int pos);
			~NeighborVectorIter(void);

			bool operator!=(const NeighborVectorIter &other);
			int operator*(void) const;
			const NeighborVectorIter& operator++(void);

		private:
			int pos;
			const NeighborVector *vec;
	};
} /* graph */

#endif /* end of include guard: ITERATOR_HPP_XNI0FOU1 */
