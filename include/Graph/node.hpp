#ifndef NODE_HPP_U3OULHBQ
#define NODE_HPP_U3OULHBQ

#include "Graph/iterator.hpp"

namespace graph
{
	class NeighborVectorIter;

	class NeighborVector
	{
		public:
			NeighborVector(void);
			NeighborVector(const int nbNeighbor);
			~NeighborVector(void);

			void init(const int nbNeighbor);

			bool append(const int pos);
			int count(void) const;

			NeighborVectorIter begin(void) const;
			NeighborVectorIter end(void) const;

			friend class NeighborVectorIter;

		private:
			//Maximum of neighbor a node can have:
			int nbNeighbor;
			//Actual number of neighbor it has:
			int effectiveNeighbor;
			//Our neighbor:
			int *neighbor;

	};

	struct Node {
		int x, y, id;
		NeighborVector neighbors;

		Node(void);
		Node(const int nbNeighbor);

		void setNeighborVector(const int nbNeighbor);
	};
} /* graph */

#endif /* end of include guard: NODE_HPP_U3OULHBQ */
