#ifndef FINDER_HPP_AFBW9K8U
#define FINDER_HPP_AFBW9K8U

#include <tuple>
#include <queue>
#include <vector>
#include <algorithm>

#include <iostream>
#include <cstdio>

#include "Graph/graph.hpp"

namespace pathfinder
{
	class RectangleMap : public graph::Graph {
		public:
			RectangleMap(const unsigned char *map, const int nMapWidth, const int nMapHeight);
			virtual std::tuple<int,int> fromFlattenIdx(const int pos) const override;
			virtual inline int toFlattenIdx(const int x, const int y) const override;

			virtual bool contain(const int x, const int y) const override;
		private:
			const int map_w, map_h;
	};

	int BreadthFirstSearch(
			const int nStartX, const int nStartY,					// Starting point
			const int nTargetX, const int nTargetY,					// Destination point
			const unsigned char* pMap, const int nMapWidth, const int nMapHeight,	// Grid of given size with row-major
			int* pOutBuffer, const int nOutBufferSize				// Output buffer for the result
	);
	// Class and function.
	int FindPath(
			const int nStartX, const int nStartY,					// Starting point
			const int nTargetX, const int nTargetY,					// Destination point
			const unsigned char* pMap, const int nMapWidth, const int nMapHeight,	// Grid of given size with row-major
			int* pOutBuffer, const int nOutBufferSize				// Output buffer for the result
	);											// Length of path or -1 if failed
} /* PathFinder */

#endif /* end of include guard: FINDER_HPP_AFBW9K8U */
